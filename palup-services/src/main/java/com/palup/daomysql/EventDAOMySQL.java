package com.palup.daomysql;

import com.palup.dao.EventDAO;
import com.palup.dao.LabelDAO;
import com.palup.daomysql.columns.EventColumn;
import com.palup.entity.Event;
import com.palup.entity.EventBrief;
import com.palup.entity.EventCreationDetails;
import com.palup.entity.Location;
import com.palup.utilities.MapUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * MySQL implementation for EventDAO
 * <p>
 * Created by  Kuldeep Yadav on 21-Oct-15.
 */
public class EventDAOMySQL extends MySQLAccess implements EventDAO {

    /**
     * Data access object to retrieve labels
     */
    private LabelDAO labelDAO;

    public EventDAOMySQL(String user, String password, String url) throws SQLException, ClassNotFoundException {
        super(user, password, url);
        labelDAO = new LabelDAOMySQL(user, password, url);
    }

    public EventDAOMySQL() throws SQLException, ClassNotFoundException {
        super();
        labelDAO = new LabelDAOMySQL();
    }

    @Override
    public void createEvent(EventCreationDetails event) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("INSERT INTO paldb.events VALUES (default, ?, ?, ?, ?, ?, ?)");

        statement.setString(EventColumn.TITLE.ordinal(), event.getTitle());
        statement.setLong(EventColumn.TIME.ordinal(), event.getTime());
        statement.setDouble(EventColumn.LATITUDE.ordinal(), event.getLocation().getLatitude());
        statement.setDouble(EventColumn.LONGITUDE.ordinal(), event.getLocation().getLongitude());
        statement.setString(EventColumn.DESCRIPTION.ordinal(), event.getDescription());
        statement.setString(EventColumn.LABEL.ordinal(), event.getLabel());
        statement.executeUpdate();
    }

    @Override
    public Event readEvent(long id) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("SELECT * FROM paldb.events WHERE id = ?");
        statement.setDouble(1, id);

        ResultSet result = statement.executeQuery();
        result.next();
        String title = result.getString(EventColumn.TITLE.toString());
        String description = result.getString(EventColumn.DESCRIPTION.toString());
        double latitude = result.getDouble(EventColumn.LATITUDE.toString());
        double longitude = result.getDouble(EventColumn.LONGITUDE.toString());
        long time = result.getLong(EventColumn.TIME.toString());
        String labelId = result.getString(EventColumn.LABEL.toString());
        return new Event(id, title, time, new Location(latitude, longitude), description, labelId);
    }

    @Override
    public void deleteEvent(long id) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("DELETE FROM paldb.events WHERE id = ?");
        statement.setDouble(1, id);
        statement.executeUpdate();
    }

    @Override
    public List<EventBrief> readBriefEvents() throws SQLException {

        PreparedStatement statement = connection.prepareStatement("SELECT * FROM paldb.events");
        ResultSet result = statement.executeQuery();
        return getEventsFromResult(result);
    }

    @Override
    public List<EventBrief> readBriefEvents(String label) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("SELECT * FROM paldb.events WHERE LABEL = ?");
        statement.setString(1, label);
        ResultSet result = statement.executeQuery();
        return getEventsFromResult(result);
    }

    /**
     * @param result @ResultSet containing rows of event table
     * @return list of brief events
     * @throws SQLException
     */
    private List<EventBrief> getEventsFromResult(ResultSet result) throws SQLException {

        List<EventBrief> events = new LinkedList<>();
        while (result.next()) {
            long id = result.getLong(EventColumn.ID.toString());
            String title = result.getString(EventColumn.TITLE.toString());
            double latitude = result.getDouble(EventColumn.LATITUDE.toString());
            double longitude = result.getDouble(EventColumn.LONGITUDE.toString());
            long time = result.getLong(EventColumn.TIME.toString());
            String labelId = result.getString(EventColumn.LABEL.toString());
            events.add(new EventBrief(id, title, time, new Location(latitude, longitude), labelId));
        }
        return events;
    }

    @Override
    public List<EventBrief> readBriefEvents(List<String> labels) throws SQLException, ClassNotFoundException {

        String commaSeperatedLabelSet = "";
        for (String label : labels) {
            commaSeperatedLabelSet += label;
            commaSeperatedLabelSet += ",";
        }

        PreparedStatement statement = connection.prepareStatement("SELECT * FROM paldb.events WHERE FIND_IN_SET(LABEL, ?)");
        statement.setString(1, commaSeperatedLabelSet);
        ResultSet result = statement.executeQuery();
        return getEventsFromResult(result);
    }

    @Override
    public List<EventBrief> readRelevantEvents(List<String> labels,
                                               MapUtils.Pair<Double> latitudeBound,
                                               MapUtils.Pair<Double> longitudeBound) throws SQLException {

        PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM paldb.events WHERE (LATITUDE BETWEEN ? AND ?) AND (LONGITUDE BETWEEN ? AND ?) AND FIND_IN_SET(LABEL, ?)");
        statement.setDouble(1, latitudeBound.getFirst());
        statement.setDouble(2, latitudeBound.getSecond());
        statement.setDouble(3, longitudeBound.getFirst());
        statement.setDouble(4, longitudeBound.getSecond());

        String commaSeparatedLabelSet = "";
        for (String label : labels) {
            commaSeparatedLabelSet += label;
            commaSeparatedLabelSet += ",";
        }

        statement.setString(5, commaSeparatedLabelSet);

        ResultSet result = statement.executeQuery();
        return getEventsFromResult(result);
    }
}
