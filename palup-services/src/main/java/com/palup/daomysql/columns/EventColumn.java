package com.palup.daomysql.columns;

/**
 * Columns for events table.
 * <p>
 * Created by  Kuldeep Yadav on 23-Oct-15.
 */
public enum EventColumn {

    // Order of the enum elements is to be kept
    // consistent with columns of event table
    ID,
    TITLE,
    TIME,
    LATITUDE,
    LONGITUDE,
    DESCRIPTION,
    LABEL
}
