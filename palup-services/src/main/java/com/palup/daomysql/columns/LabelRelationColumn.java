package com.palup.daomysql.columns;

/**
 * Columns in label relation table
 * <p>
 * Created by Kuldeep Yadav on 08-Dec-15.
 */
public enum LabelRelationColumn {
    PARENT, CHILD
}
