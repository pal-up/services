package com.palup.daomysql.columns;

/**
 * Columns for interest table
 * <p>
 * Created by kuldeep on 28/12/15.
 */
public enum InterestColumn {

    // Order of the enum elements is to be kept
    // consistent with columns of event table
    EMAIL,
    LABEL;

    public int columnNumber() {
        return ordinal() + 1;
    }
}
