package com.palup.daomysql;

import com.palup.dao.UserDAO;
import com.palup.daomysql.columns.InterestColumn;
import com.palup.daomysql.columns.UserColumn;
import com.palup.entity.UserCreationDetails;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * MySQL implementation of {@link UserDAO}
 * <p>
 * Created by kuldeep on 15/12/15.
 */
public class UserDAOMySQL extends MySQLAccess implements UserDAO {

    public UserDAOMySQL(String user, String password, String url)
            throws ClassNotFoundException, SQLException {
        super(user, password, url);
    }

    public UserDAOMySQL() throws SQLException, ClassNotFoundException {
        super();
    }

    @Override
    public void create(UserCreationDetails details) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("INSERT INTO paldb.users VALUES (?, ?, ?)");

        statement.setString(UserColumn.EMAIL.columnNumber(), details.getEmail());
        statement.setString(UserColumn.NAME.columnNumber(), details.getName());
        statement.setString(UserColumn.PHOTO_URL.columnNumber(), details.getPhotoUrl());
        statement.executeUpdate();
    }

    @Override
    public void update(UserCreationDetails details) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("UPDATE paldb.users SET photo_url = ? WHERE email = ?");
        statement.setString(1, details.getPhotoUrl());
        statement.setString(2, details.getEmail());
        statement.executeUpdate();
    }

    @Override
    public boolean isExistingUser(String email) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM paldb.users WHERE email = ?");
        statement.setString(1, email);
        ResultSet result = statement.executeQuery();
        result.next();
        return result.getInt(1) == 1;
    }

    @Override
    public List<String> userInterest(String email) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("SELECT label FROM paldb.interest WHERE email = ?");
        statement.setString(1, email);
        ResultSet result = statement.executeQuery();
        List<String> labels = new ArrayList<>();
        while (result.next()) {
            String label = result.getString(InterestColumn.LABEL.toString());
            labels.add(label);
        }
        return labels;
    }

    @Override
    public void addInterest(String email, List<String> labels) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("INSERT INTO paldb.interest VALUES (?, ?)");
        for (String label : labels) {
            statement.setString(1, email);
            statement.setString(2, label);
            statement.addBatch();
        }
        statement.executeBatch();
    }

    @Override
    public void removeInterest(String email, List<String> labels) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("DELETE FROM paldb.interest WHERE email = ? AND label = ?");
        for (String label : labels) {
            statement.setString(1, email);
            statement.setString(2, label);
            statement.addBatch();
        }
        statement.executeBatch();
    }
}
