package com.palup.dao;

import com.palup.entity.UserCreationDetails;

import java.sql.SQLException;
import java.util.List;

/**
 * User CRUD APIs
 * <p>
 * Created by kuldeep on 15/12/15.
 */
public interface UserDAO {

    /**
     * @param details of the user to be created
     */
    void create(UserCreationDetails details) throws SQLException;

    /**
     * @param details of user to be updated
     * @throws SQLException
     */
    void update(UserCreationDetails details) throws SQLException;

    /**
     * @param email of expected user
     * @return true if user exists in database, otherwise false
     * @throws SQLException
     */
    boolean isExistingUser(String email) throws SQLException;

    /**
     * @param email of user
     * @return list of interest labels for the user
     * @throws SQLException
     */
    List<String> userInterest(String email) throws SQLException;

    /**
     * Add given interests for the user
     *
     * @param email  id of user
     * @param labels to be added in user interest
     * @throws SQLException
     */
    void addInterest(String email, List<String> labels) throws SQLException;

    /**
     * Remove given interests for the user
     *
     * @param email  id of user
     * @param labels to remove
     */
    void removeInterest(String email, List<String> labels) throws SQLException;
}
