package com.palup.dao;

import com.palup.entity.Label;
import com.palup.entity.LabelCreationDetails;

import java.sql.SQLException;
import java.util.List;

/**
 * Label CRUD APIs
 * <p>
 * Created by Kuldeep Yadav on 08-Dec-15.
 */
public interface LabelDAO {

    /**
     * @param label details to create label
     * @return new created label
     * @throws SQLException
     */
    Label createLabel(LabelCreationDetails label) throws SQLException;

    /**
     * Set parent child relation between labels
     *
     * @param label creation details
     * @throws SQLException
     */
    void setLabelRelation(LabelCreationDetails label) throws SQLException;

    /**
     * @param id of the label
     * @return label
     * @throws SQLException
     */
    Label readLabel(String id) throws SQLException;

    /**
     * @return list of available labels
     * @throws SQLException
     */
    List<Label> readLabels() throws SQLException;
}
