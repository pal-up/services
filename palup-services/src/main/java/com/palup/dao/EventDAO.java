package com.palup.dao;

import com.palup.entity.Event;
import com.palup.entity.EventBrief;
import com.palup.entity.EventCreationDetails;
import com.palup.utilities.MapUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * Event CRUD APIs.
 * <p>
 * Created by  Kuldeep Yadav on 21-Oct-15.
 */
public interface EventDAO {

    /**
     * Create new event with the given details
     *
     * @param event details.
     */
    void createEvent(EventCreationDetails event) throws SQLException;

    /**
     * @param id of event
     * @return the event with given id.
     */
    Event readEvent(long id) throws SQLException;

    /**
     * Delete event.
     *
     * @param id of the event.
     */
    void deleteEvent(long id) throws SQLException;

    /**
     * All events.
     *
     * @return list of all events
     * @throws SQLException
     */
    List<EventBrief> readBriefEvents() throws SQLException;

    /**
     * Read all events with specified label
     *
     * @param label on events to fetch
     * @return list of events
     * @throws SQLException
     */
    List<EventBrief> readBriefEvents(String label) throws SQLException;

    /**
     * Read events with specified labels
     *
     * @param labels on events to be read
     * @return events with specified labels
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    List<EventBrief> readBriefEvents(List<String> labels) throws SQLException, ClassNotFoundException;

    /**
     * Read events with specified labels which fall in rectangular
     * area formed by latitude and longitude bound.
     *
     * @param labels         on events to be read
     * @param latitudeBound  bound(min, max) on latitude
     * @param longitudeBound bound(min, max) on latitude
     * @return list of events which satisfy the following conditions
     * event have a label present in given list,
     * latitude falls in given bound,
     * longitude falls in given bound
     * @throws SQLException
     */
    List<EventBrief> readRelevantEvents(List<String> labels,
                                        MapUtils.Pair<Double> latitudeBound,
                                        MapUtils.Pair<Double> longitudeBound) throws SQLException;
}
