package com.palup.daomysql;

import com.palup.dao.EventDAO;
import com.palup.entity.Event;
import com.palup.entity.EventBrief;
import com.palup.entity.EventCreationDetails;
import com.palup.entity.Location;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Junit test for EventDAOMySQL
 * <p>
 * Created by  Kuldeep Yadav on 22-Oct-15.
 */
public class EventDAOMySQLTest {

    /**
     * User name to access database.
     */
    private String user = "kuldeep";

    /**
     * Password for the user
     */
    private String password = "kd5534kd";

    /**
     * Database to access
     */
    private String url = "jdbc:mysql://localhost/paldb";

    @Test
    public void testCreateEvent() throws Exception {

        EventDAO eventDAO = new EventDAOMySQL(user, password, url);
        Location location = new Location(12.9667, 77.5667);
        EventCreationDetails eventCreationDetails = new EventCreationDetails("Sampurna's marriage", "Sampurna's marriage is fixed with Mr 007 James Bond", location, 1445522471853L, "marriage");
        eventDAO.createEvent(eventCreationDetails);
    }

    //    @Test
    public void testReadEvent() throws Exception {

        EventDAO eventDAO = new EventDAOMySQL(user, password, url);
        Event event = eventDAO.readEvent(3);
        Assert.assertEquals(3, event.getId());
    }

    //    @Test
    public void testDeleteEvent() throws Exception {

        EventDAO eventDAO = new EventDAOMySQL(user, password, url);
        eventDAO.deleteEvent(7);
    }

    @Test
    public void testReadBriefEvents() throws SQLException, ClassNotFoundException {

        EventDAO eventDAO = new EventDAOMySQL(user, password, url);
        for (EventBrief event : eventDAO.readBriefEvents()) {
            System.out.println(event.toString());
        }
    }
}